import sklearn.datasets as datasets
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

data = datasets.load_iris()

X = data.data
Y = data.target

print("N data:", len(X))
print("N labels:", len(Y))

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)

print("N data train:", len(X_train))
