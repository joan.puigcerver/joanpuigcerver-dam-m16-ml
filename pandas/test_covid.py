import pandas as pd
import matplotlib.pyplot as plt
import io
import requests

#url = "https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/owid-covid-data.csv"
#content = requests.get(url).content
#data = pd.read_csv(io.StringIO(content.decode("utf-8")))
data = pd.read_csv("../data/spain.csv")

plt.plot(data["total_cases"])
plt.show()
