import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv("../data/titanic.csv")

data_female = data.loc[data["Sex"] == "female", "Age"]
data_male = data.loc[data["Sex"] == "male", "Age"]

plt.hist([data_male, data_female], range=[0,80], bins=80, label=["Male","Female"])
plt.xlabel("Edat")
plt.ylabel("Freqüència")
plt.legend()
plt.show()