import pandas as pd

data = pd.read_csv("../data/titanic.csv")

# Exercici 1
print(data.shape)

# Exercici 2
print(data.info())

# Exercici 3
print(data.head())

# Exercici 4
print(data[ ["Name", "Sex", "Age"] ].head())

# Exercici 5
menors_30 = data[ data["Age"] < 30 ]
print(menors_30.shape)

# Exercici 6
print( data.loc[data["Age"] < 30, ["Name", "Sex", "Age"]] )
print( data[data["Age"] < 30][["Name", "Sex", "Age"]] )

# Exercici 7
print(data["Age"].min())

# Exercici 8
edat_max = data["Age"].max()
print(data.loc[data["Age"] == edat_max, ["Name", "Age"]])

# Exercici 9
supervivents = data[data["Survived"] == 1]
print(supervivents["Age"].mean())
