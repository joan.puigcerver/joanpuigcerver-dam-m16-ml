def reverse(string):
    r = ""
    for c in reversed(string):
        r += c
    return r

def es_palindom(string):
    return string == string[::-1]

def es_palindom2(string):
    i = 0
    j = len(string) - 1
    while i <= j :
        if string[i] != string[j] :
            return False
        i += 1
        j -= 1
    return True

paraula = input()
print(es_palindom(paraula))
print(es_palindom2(paraula))
