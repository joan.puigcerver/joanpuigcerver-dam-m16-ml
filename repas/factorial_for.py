n = int(input("Introdueix el valor: "))

factorial = 1
for current in range(2, n+1):
    factorial *= current
    print("Current: {}, Factorial: {}".format(current, factorial))

print(factorial)