def maxim_llista(llista):
    maxim = llista[0]
    for element in llista:
        if element > maxim:
            maxim = element
    return maxim

entrada = input()
llista = [int(x) for x in entrada.split(",")]

maxim = maxim_llista(llista)
print(maxim)
print(max(llista))
