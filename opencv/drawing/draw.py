import cv2 as cv
import numpy as np

itb = cv.imread("../img/itb.jpg")

# BGR
itb = cv.rectangle(itb, (140, 140), (410, 410), (0, 0, 255), thickness=-1)
itb = cv.circle(itb, (275, 275), 135, (0, 255, 255), thickness=-1)
itb = cv.line(itb, (426, 224), (767, 217), (229, 104, 84), thickness=5)
itb = cv.rectangle(itb, (455, 420), (520, 450), (242, 244, 244), thickness=-1)
itb = cv.putText(itb, '777', (455, 450), cv.FONT_HERSHEY_TRIPLEX, 1.0, (0,0,0), thickness=2)

cv.imshow("ITB", itb)
cv.waitKey(0)