import numpy as np
import cv2 as cv

def overlay(img, img2, pos, alpha=1, mask=None):
    """ Overlays an image (img2) on another image (img) on the point (x, y)

    :param img: Base image
    :param img2: Overlay image
    :param pos: (x, y) Position in pixels
    :param alpha: Blending factor
    :param mask: Mask applied to img2
    :return:
    """
    dst=np.copy(img)
    dst2=np.copy(img2)

    img_h, img_w = img.shape[:2]
    img2_h, img2_w = img2.shape[:2]

    x, y = pos
    if x < 0:
        x = img_w + x
    if y < 0:
        y = img_h + y

    x1 = max(0, x)
    x2 = min(img_w, x1 + img2_w)
    y1 = max(0, y)
    y2 = min(img_h, y1 + img2_h)

    original = img[y1:y2, x1:x2]
    dst2 = dst2[0:(y2-y1), 0:(x2-x1)]

    result = cv.addWeighted(dst2, alpha, original, (1-alpha), 0.0)

    if mask is not None:
        mask = mask[0:(y2-y1), 0:(x2-x1)]
        inv_mask = cv.bitwise_not(mask)
        cv.imshow("Original", original)
        original = cv.bitwise_and(original, original, mask=inv_mask)
        cv.imshow("Original_mask", original)
        result = cv.bitwise_and(result, result, mask=mask)
        cv.imshow("Result_mask", result)
        result = cv.add(result, original)
        cv.imshow("Result", result)

    dst[y1:y2, x1:x2] = result
    return dst
