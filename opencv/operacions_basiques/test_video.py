import cv2
import numpy as np

capture = cv2.VideoCapture(0)
cv2.namedWindow('Camara')

while(1):
    isTrue, img = capture.read()

    cv2.imshow('Camara', img)

    k = cv2.waitKey(1) & 0xFF
    if k == 27: # ESC
        break

cv2.destroyAllWindows()