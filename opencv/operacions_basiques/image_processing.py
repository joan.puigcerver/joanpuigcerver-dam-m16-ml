import cv2
import numpy as np


def nothing(x):
    pass

capture = cv2.VideoCapture(0)
original_img = cv2.imread("../img/traffic.png")
cv2.namedWindow('Camara')

# create trackbars for color change
cv2.createTrackbar('gray','Camara', 0, 1, nothing)
cv2.createTrackbar('threshold','Camara', -1, 255, nothing)
cv2.createTrackbar('kernel blur (2n -1)', 'Camara', -1, 50, nothing)
cv2.createTrackbar('gaussian kernel blur (2n -1)', 'Camara', -1, 50, nothing)
cv2.createTrackbar('laplacian', 'Camara', 0, 1, nothing)
cv2.createTrackbar('canny_min', 'Camara', 0, 255, nothing)
cv2.createTrackbar('canny_max', 'Camara', 0, 255, nothing)
cv2.createTrackbar('contours', 'Camara', 0, 1, nothing)

colors = []
colors.append((255,0,0))
colors.append((0,0,255))
colors.append((255,255,0))
colors.append((255,255,0))
colors.append((0,255,255))
colors.append((255,0,255))

while(1):
    # isTrue, img = capture.read()
    img = original_img

    # get current positions of four trackbars
    gray = cv2.getTrackbarPos('gray', 'Camara')
    threshold = cv2.getTrackbarPos('threshold', 'Camara')
    blur = cv2.getTrackbarPos('kernel blur (2n -1)', 'Camara')
    gauss_blur = cv2.getTrackbarPos('gaussian kernel blur (2n -1)', 'Camara')
    laplacian = cv2.getTrackbarPos('laplacian', 'Camara')
    canny_min = cv2.getTrackbarPos('canny_min', 'Camara')
    canny_max = cv2.getTrackbarPos('canny_max', 'Camara')
    countours = cv2.getTrackbarPos('contours', 'Camara')
    blur = 2 * blur + 1
    gauss_blur = 2 * gauss_blur + 1

    is_gray = gray > 0 or threshold > 0 or laplacian > 0
    if is_gray:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if blur > 0:
        img = cv2.blur(img, (blur, blur))
    if gauss_blur > 0:
        img = cv2.GaussianBlur(img, (gauss_blur, gauss_blur), 0)

    if threshold > 0:
        _, img = cv2.threshold(img, threshold, 255, cv2.THRESH_BINARY)

    if laplacian > 0:
        img = cv2.Laplacian(img, cv2.CV_64F)
        img = np.uint8(np.absolute(img))
        img = cv2.bitwise_not(img)

    if canny_min > 0:
        img = cv2.Canny(img, canny_min, max(canny_min, canny_max))

    if countours > 0:
        if is_gray or canny_min > 0:
            gray_img = img
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        else:
            gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        contours, hierarchy = cv2.findContours(gray_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for i, contour in enumerate(contours) :
            color = colors[i % len(colors)]
            img = cv2.drawContours(img, [contour], 0, color, 2)

    cv2.imshow('Camara', img)

    k = cv2.waitKey(1) & 0xFF
    if k == 27: # ESC
        break

cv2.destroyAllWindows()