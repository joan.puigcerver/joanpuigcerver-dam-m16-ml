import cv2 as cv
import numpy as np
from opencv.transformations import resize, translate, rotate, prespective

itb = cv.imread("../img/itb.jpg")
cv.imshow("ITB", itb)

resized = cv.resize(itb, (300, 300), interpolation=cv.INTER_AREA)
resized = resize(itb, 0.70710678118)
cv.imshow("Resized", resized)

translated = translate(itb, 100, 100)
cv.imshow("Translated", translated)

rotated = rotate(itb, -90, rotPoint=(280, 280))
cv.imshow("Rotated", rotated)

flipped = cv.flip(itb, -1)
cv.imshow("Flipped", flipped)

cropped = itb[140:410, 140:410]
cv.imshow("cropped", cropped)

prespectived = prespective(itb, (115, 115), (878, 95), (128, 498), (880, 468), 1000, 500)
cv.imshow("Prespective", prespectived)

cv.waitKey(0)