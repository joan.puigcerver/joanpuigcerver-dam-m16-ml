from facenet_pytorch import MTCNN
import cv2 as cv
import torch

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

mtcnn = MTCNN(keep_all=True, device=device)

capture = cv.VideoCapture(0)
# capture.set(3, 1280)
# capture.set(4, 720)

while True:
    isTrue, frame = capture.read()
    # gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    boxes, probs, points = mtcnn.detect(frame,landmarks=True)

    if boxes is not None:
        for (box, point) in zip(boxes, points) or []:
            (x, y, x2, y2) = box.astype('int')
            p1 = (x , y)
            p2 = (x2, y2)
            frame = cv.rectangle(frame, p1, p2, (0, 255, 0), thickness=2)
            for p in point:
                p = tuple(p.astype('int'))
                frame = cv.circle(frame, p, 5, (0, 0, 255), thickness=2)

    cv.imshow("Video", frame)

    if cv.waitKey(20) & 0xFF==ord('d'):
        break

capture.release()
cv.destroyAllWindows()
