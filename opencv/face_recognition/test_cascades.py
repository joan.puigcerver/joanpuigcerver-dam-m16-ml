# https://www.pyimagesearch.com/2017/04/10/detect-eyes-nose-lips-jaw-dlib-opencv-python/
# https://towardsdatascience.com/real-time-face-recognition-an-end-to-end-project-b738bb0f7348
import cv2 as cv

faceCascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_frontalface_default.xml')
eyesCascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_eye.xml')

capture = cv.VideoCapture(0)

while True:
    isTrue, frame = capture.read()
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # faces = detector.detect_faces(frame)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(20, 20)
    )
    eyes = eyesCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5
    )

    for (x, y, w, h) in faces :
        p1 = (x, y)
        p2 = (x + w, y + h)
        frame = cv.rectangle(frame, p1, p2, (0, 255, 0), thickness=2)

    for (x, y, w, h) in eyes:
        p1 = (x, y)
        p2 = (x + w, y + h)
        frame = cv.rectangle(frame, p1, p2, (0, 0, 255), thickness=2)

    cv.imshow("Video", frame)

    if cv.waitKey(30) & 0xFF==ord('d'):
        break

capture.release()
cv.destroyAllWindows()
