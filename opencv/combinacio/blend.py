import cv2 as cv
import numpy as np

itb = cv.imread("../img/itb.jpg")
itb_h, itb_w = itb.shape[:2]
cv.imshow("ITB", itb)

logo = itb[140:410, 140:410]
logo_h, logo_w = logo.shape[:2]
cv.imshow("Logo", logo)
print(logo.shape)
print(itb.shape)


x = itb_w - logo_w
y = 0
original = itb[y:y+logo_h, x:x+logo_w]
cv.imshow("original", original)

alpha = 1
resultat = cv.addWeighted(logo, alpha, original, (1-alpha), 0.0);

# cv.imshow("Resultat", resultat)

itb[y:y+logo_h, x:x+logo_w] = resultat
cv.imshow("Blended", itb)

cv.waitKey(0)