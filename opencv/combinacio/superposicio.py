import cv2 as cv
from opencv.combinations import overlay
from opencv.transformations import resize

itb = cv.imread("../img/itb.jpg")
itb_h, itb_w = itb.shape[:2]
# cv.imshow("ITB", itb)

logo = itb[140:410, 140:410].copy()
logo_h, logo_w = logo.shape[:2]
# cv.imshow("Logo", logo)
print(logo.shape)
print(itb.shape)

x1 = 200
y1 = 200
x2 = x1 + logo_w
y2 = y1 + logo_h

itb[y1:y2, x1:x2] = logo.copy()

itb = overlay(itb, logo, (500, 100), alpha=0.5)
cv.imshow("Superposition", itb)

cv.waitKey(0)