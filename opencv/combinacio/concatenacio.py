import cv2 as cv
import numpy as np

itb = cv.imread("../img/itb.jpg")
cv.imshow("ITB", itb)

# Verticalment
concatenated_v = np.concatenate((itb, itb), axis=0)

# Horitzontalment
concatenated_h = np.concatenate((itb, itb), axis=1)

cv.imshow("Horitzontal", concatenated_h)
cv.imshow("Vertical", concatenated_v)

cv.waitKey(0)