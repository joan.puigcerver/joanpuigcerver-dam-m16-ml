import cv2 as cv
import numpy as np
from opencv.combinations import overlay

itb = cv.imread("../img/itb.jpg")
itb_h, itb_w = itb.shape[:2]

logo = itb[140:410, 140:410]
logo_h, logo_w = logo.shape[:2]
logo_gray = cv.cvtColor(logo, cv.COLOR_BGR2GRAY)
_, mask = cv.threshold(logo_gray, 150, 255, cv.THRESH_BINARY_INV)
# mask = cv.circle(mask, (135,135), 50, 255, -1)
cv.imshow("Mask", mask)

x = itb_w - logo_w
y = 0

itb = overlay(itb, logo, (x, y), alpha=0.5, mask=mask)
cv.imshow("ITB", itb)

cv.waitKey(0)