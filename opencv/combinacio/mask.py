import cv2 as cv
import numpy as np

itb = cv.imread("../img/itb.jpg")
cv.imshow("ITB", itb)

mask = np.zeros(itb.shape[:2], dtype='uint8')
mask = cv.rectangle(mask, (420, 310), (846, 371), 255, thickness=-1)
mask = cv.circle(mask, (275, 275), 140, 255, thickness=-1)
cv.imshow("Rectangular Mask", mask)

masked = cv.bitwise_and(itb, itb, mask=mask)
cv.imshow("Masked", masked)

cv.waitKey(0)