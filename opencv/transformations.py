import numpy as np
import cv2 as cv

def crop(src, p1, p2):
    return src[p1[1]:p2[1], p1[0]:p2[0]].copy()

def resize(src, factor, interpolation=cv.INTER_AREA):
    """ Resizes an image by the specified factor keeping the aspect ratio

    :param src: Image to be resized
    :param scale: Resize factor
    :param interpolation:
    :return: Resized image
    """
    width = src.shape[1]
    height = src.shape[0]
    dimensions = (int(width*factor), int(height*factor))
    return cv.resize(src, dimensions, interpolation)


def translate(img, x, y):
    transMat = np.float32([[1, 0, x], [0, 1, y]])
    dimensions = (img.shape[1], img.shape[0])
    return cv.warpAffine(img, transMat, dimensions)


def rotate(img, angle, rotPoint=None):
    (height, width) = img.shape[:2]

    if rotPoint is None:
        rotPoint = (width // 2, height // 2)

    rotMat = cv.getRotationMatrix2D(rotPoint, angle, 1.0)
    dimensions = (width, height)

    return cv.warpAffine(img, rotMat, dimensions)

def prespective(img, p1, p2, p3, p4, height=500, width=500):
    rows, cols, ch = img.shape

    pts1 = np.float32([[p1[0], p1[1]], [p2[0], p2[1]], [p3[0], p3[1]], [p4[0], p4[1]]])
    pts2 = np.float32([[0, 0], [height, 0], [0, width], [height, width]])

    M = cv.getPerspectiveTransform(pts1, pts2)

    dst = cv.warpPerspective(img, M, (height, width))
    return dst