import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

mtcars = pd.read_csv("../data/mtcars.csv")

plt.scatter(mtcars["wt"], mtcars["mpg"])

# Initialize model
regression_model = LinearRegression()

# Train the model using the mtcars data
regression_model.fit(X = pd.DataFrame(mtcars["wt"]), 
                     y = mtcars["mpg"])

# mpg = y_intercept + coef * wt.
intercept = regression_model.intercept_
coef = regression_model.coef_
coef_str = " + ".join(["{}*x{}".format(coef_i, i) for i, coef_i in enumerate(coef)])
print("mpg = {} + ({})".format(coef_str, intercept))

score = regression_model.score(X = pd.DataFrame(mtcars["wt"]),
                       y = mtcars["mpg"])

print("R-squared:", score)
X = pd.DataFrame(mtcars["wt"]).sort_values("wt")
y_prediction = regression_model.predict(X)
plt.plot(X, y_prediction, color="red")
plt.show()

