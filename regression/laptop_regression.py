import pandas as pd
import matplotlib.pyplot as plt
import re
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline

def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Extraction error RAM:", x)

def extract_hz(x):
    match = re.search(r"(\d+(?:\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("GHz extraction error:", x)

def extract_disk(x):
    re_mida = r"(\d+)([GT])B"
    match = re.findall(re_mida, x)

    mida = 0
    if match:
        for g in match:
            local_mida = int(g[0])
            if g[1] == "T":
                local_mida *= 1024
            mida += local_mida
    return mida


def extract_disk_segregate(x):
    re_mida = r"(\d+)([GT])B (HDD)?"
    match = re.findall(re_mida, x)

    midaHDD = 0
    midaSSD = 0
    if match:
        for g in match:
            local_mida = int(g[0])
            if g[1] == "T":
                local_mida *= 1024
            if g[2] == "HDD":
                midaHDD += local_mida
            else:
                midaSSD += local_mida
    return midaHDD, midaSSD

def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error extraent resolució", x)

# Main

laptops = pd.read_csv("../data/laptops.csv", encoding="utf-16")

laptops["Cpu"] = laptops["Cpu"].apply(extract_hz)
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["MemoryHDD"], laptops["MemorySSD"] = zip(*laptops["Memory"].map(extract_disk_segregate))
laptops["Memory"] = laptops["Memory"].apply(extract_disk)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Company"] = pd.Categorical(laptops["Company"])

def train_model(model, data):
    model.fit(data["X_train"], data["y_train"])
    return model.score(data["X_test"], data["y_test"])

def Polynomial(degree):
    return make_pipeline(PolynomialFeatures(degree), LinearRegression())

features_list = []
features_list.append(["Cpu"])
features_list.append(["Ram"])
features_list.append(["Cpu", "Ram"])
features_list.append(["Cpu", "Ram", "ScreenResolution"])
features_list.append(["Cpu", "Ram", "ScreenResolution", "Memory"])
features_list.append(["Cpu", "Ram", "ScreenResolution", "MemoryHDD", "MemorySSD"])

for features in features_list:
    X = laptops.loc[:, features]
    y = laptops["Price_euros"]
    data = {}
    data["X_train"], data["X_test"], data["y_train"], data["y_test"] = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle = True)

    print("Linial:", features, train_model(LinearRegression(), data))
    scores_poly = [(degree, train_model(Polynomial(degree), data)) for degree in range(2,10)]
    max_degree, max_score = max(scores_poly, key=lambda x:x[1])
    print("Poly {}:".format(max_degree), features, max_score)
