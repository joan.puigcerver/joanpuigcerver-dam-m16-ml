# Laptop regression

| Model |Score Trainig|Score Test|
|----------|:-------------:|:------:|
| <ul><li>2x Dense(64, relu)</li><li>1x Dense(128, relu)</li></ul> |  0.26 | 0.14 |
| <ul><li>2x Dense(64, relu)</li><li>1x Dense(128, relu)</li></ul> |  0.26 | 0.14 |
| <ul><li>2x Dense(64, relu)</li><li>1x Dense(128, relu)</li></ul> |  0.26 | 0.14 |
