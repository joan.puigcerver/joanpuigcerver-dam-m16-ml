import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LinearRegression

mtcars = pd.read_csv("../data/mtcars.csv")

plt.scatter(mtcars["wt"], mtcars["mpg"])

# Initialize model
degree = 2
regression_model = make_pipeline(PolynomialFeatures(degree), LinearRegression())

# Train the model using the mtcars data
regression_model.fit(X = pd.DataFrame(mtcars["wt"]), 
                     y = mtcars["mpg"])

# mpg = y_intercept + coef * wt.

score = regression_model.score(X = pd.DataFrame(mtcars["wt"]),
                       y = mtcars["mpg"])

print(score)
X = pd.DataFrame(mtcars["wt"]).sort_values("wt")
y_prediction = regression_model.predict(X)
plt.plot(X, y_prediction, color="red")
plt.show()

