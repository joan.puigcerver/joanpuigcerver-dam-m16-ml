import pandas as pd
import matplotlib.pyplot as plt
import re
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline

def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Extraction error RAM:", x)

def extract_hz(x):
    match = re.search(r"(\d+(?:\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("GHz extraction error:", x)

def extract_disk(x):
    re_mida = r"(\d+)([GT])B"
    match = re.findall(re_mida, x)

    mida = 0
    if match:
        for g in match:
            local_mida = int(g[0])
            if g[1] == "T":
                local_mida *= 1024
            mida += local_mida
    return mida


def extract_disk_segregate(x):
    re_mida = r"(\d+)([GT])B (HDD)?"
    match = re.findall(re_mida, x)

    midaHDD = 0
    midaSSD = 0
    if match:
        for g in match:
            local_mida = int(g[0])
            if g[1] == "T":
                local_mida *= 1024
            if g[2] == "HDD":
                midaHDD += local_mida
            else:
                midaSSD += local_mida
    return midaHDD, midaSSD

def extract_resolution(x):
    match = re.search("(\d+)x\d+", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error extraent resolució", x)


def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.yscale('log')
    plt.legend()
    plt.grid(True)
    plt.show()

# Main

laptops = pd.read_csv("../data/laptops.csv", encoding="utf-16")

laptops["Cpu"] = laptops["Cpu"].apply(extract_hz)
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["MemoryHDD"], laptops["MemorySSD"] = zip(*laptops["Memory"].map(extract_disk_segregate))
laptops["Memory"] = laptops["Memory"].apply(extract_disk)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Company"] = pd.Categorical(laptops["Company"])
laptops["Company_codes"] = laptops["Company"].cat.codes

features = ["Cpu", "Ram", "ScreenResolution", "MemoryHDD", "MemorySSD", "Company_codes"]

X = laptops.loc[:, features]
y = laptops["Price_euros"]

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle = True)

normalizer = tf.keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))


model = tf.keras.models.Sequential()
model.add(normalizer)

model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))

model.add(tf.keras.layers.Dense(1))
model.summary()

model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-4),
              loss="mean_squared_error",
              metrics=tfa.metrics.RSquare(y_shape=(1,)))

history = model.fit(X_train, y_train, epochs=1000, validation_split=0.1)

loss_training, r2_training = model.evaluate(X_train, y_train)
loss, r2 = model.evaluate(X_test, y_test)
print("RSquared Training ANN:", r2_training)
print("RSquared Test ANN:", r2)
plot_loss(history)

liniar_regression = LinearRegression()
liniar_regression.fit(X_train, y_train)
r2_training = liniar_regression.score(X_train, y_train)
r2_test = liniar_regression.score(X_test, y_test)
print("Liniar regression: Test ({}) Train ({})".format(r2_test, r2_training))

# print(model.predict(np.array([1.2, 8, 1920]).reshape(1,3), batch_size=1))