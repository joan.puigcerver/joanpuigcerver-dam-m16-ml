import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np

mtcars = pd.read_csv("../data/mtcars.csv")

plt.scatter(mtcars["wt"], mtcars["mpg"])

### Regression sklearn
# Initialize model
regression_model = LinearRegression()

# Train the model using the mtcars data
X = pd.DataFrame(mtcars["wt"])
y = mtcars["mpg"]
regression_model.fit(X, y)

# mpg = y_intercept + coef * wt.
intercept = regression_model.intercept_
coef = regression_model.coef_
coef_str = " + ".join(["{}*x{}".format(coef_i, i) for i, coef_i in enumerate(coef)])
print("mpg = {} + ({})".format(coef_str, intercept))

score = regression_model.score(X = pd.DataFrame(mtcars["wt"]),
                       y = mtcars["mpg"])

### ANN
normalizer = tf.keras.layers.experimental.preprocessing.Normalization(input_shape=[1,])
normalizer.adapt(np.array(X))

ann_model = tf.keras.Sequential()
ann_model.add(normalizer)
ann_model.add(tf.keras.layers.Dense(10, activation='relu'))
ann_model.add(tf.keras.layers.Dense(10, activation='relu'))
ann_model.add(tf.keras.layers.Dense(1))

ann_model.compile(
    optimizer=tf.optimizers.Adam(learning_rate=0.1),
    loss='mean_squared_error',
    metrics=tfa.metrics.RSquare(y_shape=(1,))
)

ann_model.fit(X, y, epochs=150)
_, ann_score = ann_model.evaluate(X, y)

print("R-squared sklearn:", score)
print("R-squared ANN:", ann_score)

X = X.sort_values("wt")
y_prediction = regression_model.predict(X)
y_prediction_ann = ann_model.predict(X)
plt.plot(X, y_prediction, color="red")
plt.plot(X, y_prediction_ann, color="green")
plt.show()

