import pandas as pd
def train_test_split(*dataframes, frac=0.8, random_state=None):
    result = []
    i = 0
    train = dataframes[0].sample(frac=frac, random_state=random_state)
    index = train.index
    test = dataframes[0].drop(index)
    result.append(train)
    result.append(test)

    i+= 1
    while i < len(dataframes):
        train = dataframes[i][index]
        test = dataframes[i].drop(index)
        result.append(train)
        result.append(test)
        i += 1
    return result
