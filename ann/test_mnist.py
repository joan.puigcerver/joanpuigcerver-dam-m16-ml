import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)

model = tf.keras.models.load_model("mnist.model")

i = np.random.randint(len(x_test))
plt.imshow(x_test[i], cmap=plt.cm.binary)
plt.show()

im = x_test[i]
prediction = model.predict(im.reshape(1,-1), batch_size=1)

print("Mostra n:", i)
print("Classe real:", y_test[i])
print(prediction)
print("Classe predida:", np.argmax(prediction))