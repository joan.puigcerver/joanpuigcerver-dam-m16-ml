import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import imageio
from skimage.color import rgb2gray

mnist = tf.keras.datasets.mnist

model = tf.keras.models.load_model("mnist.model")


im = imageio.imread("/home/joapuiib/im.png")
x_test = 1 - np.array(rgb2gray(im))
plt.imshow(x_test, cmap=plt.cm.binary)
plt.show()
x_test = x_test.reshape((1,28,28))
prediction = model.predict(x_test)

print(prediction)
print("Classe predida:", np.argmax(prediction))