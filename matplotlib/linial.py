import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-5, 5, 100)
print(x)

y_linial = x

# Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
plt.plot(x, y_linial, "-", label='linear')  # Plot some data on the axes.
plt.xlabel('x label')  # Add an x-label to the axes.
plt.ylabel('y label')  # Add a y-label to the axes.
plt.title("Simple Plot")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.axhline(y=0, color='k')
plt.axvline(x=0, color='k')
plt.show()
